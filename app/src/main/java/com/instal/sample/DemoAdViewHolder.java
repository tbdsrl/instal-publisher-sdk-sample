package com.instal.sample;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.instal.nativeads.NativeErrorCode;
import com.instal.nativeads.NativeResponse;
import com.instal.nativeads.adapter.InstalAdViewHolder;
import com.squareup.picasso.Picasso;

public class DemoAdViewHolder extends InstalAdViewHolder {

    private Context context;
    private View adLayout;
    private View progress;
    private TextView titleView;
    private TextView promoView;
    private Button callToActionView;
    private RatingBar ratingBar;
    private ImageView image;

    public DemoAdViewHolder(View view) {
        super(view);
        context = view.getContext();
        adLayout = view.findViewById(R.id.ad_layout);
        progress = view.findViewById(R.id.progress);
        titleView = (TextView) view.findViewById(R.id.title);
        promoView = (TextView) view.findViewById(R.id.promo_text);
        callToActionView = (Button) view.findViewById(R.id.call_to_action);
        ratingBar = (RatingBar) view.findViewById(R.id.rating);
        image = (ImageView) view.findViewById(R.id.image);
    }

    public void onLoad(NativeResponse nativeResponse) {
        progress.setVisibility(View.GONE);
        adLayout.setVisibility(View.VISIBLE);

        titleView.setText(nativeResponse.getTitle());
        promoView.setText(nativeResponse.getPromoText());
        callToActionView.setText(nativeResponse.getCallToAction());
        Float rating = nativeResponse.getRating();
        if (rating != null) {
            ratingBar.setVisibility(View.VISIBLE);
            ratingBar.setRating(rating);
        } else {
            ratingBar.setVisibility(View.GONE);
        }

        Picasso.with(context).load(nativeResponse.getIconImageUrl()).into(image);

        nativeResponse.registerView(adLayout, callToActionView);
    }

    public void onFail(NativeErrorCode errorCode) {
        Toast.makeText(context, "Error " + errorCode, Toast.LENGTH_LONG).show();
        progress.setVisibility(View.GONE);
        adLayout.setVisibility(View.GONE);
    }

    public void onStartLoading() {
        progress.setVisibility(View.VISIBLE);
        adLayout.setVisibility(View.GONE);
        NativeResponse.unregisterView(adLayout, callToActionView);
    }
}
