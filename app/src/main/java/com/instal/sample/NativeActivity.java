package com.instal.sample;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.instal.nativeads.InstalNativeAd;
import com.instal.nativeads.InstalNativeAdListener;
import com.instal.nativeads.NativeErrorCode;
import com.instal.nativeads.NativeResponse;
import com.squareup.picasso.Picasso;


public class NativeActivity extends Activity {

    private InstalNativeAd instalNativeAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_native);

        final View adLayout = findViewById(R.id.ad_layout);
        final View progress = findViewById(R.id.progress);
        final TextView titleView = (TextView) findViewById(R.id.title);
        final TextView promoView = (TextView) findViewById(R.id.promo_text);
        final Button callToActionView = (Button) findViewById(R.id.call_to_action);
        final RatingBar ratingBar = (RatingBar) findViewById(R.id.rating);
        final ImageView image = (ImageView) findViewById(R.id.image);

        progress.setVisibility(View.VISIBLE);
        adLayout.setVisibility(View.GONE);

        instalNativeAd = new InstalNativeAd(this, "506");

        instalNativeAd.makeRequest(new InstalNativeAdListener() {
            @Override public void onLoad(NativeResponse nativeResponse) {
                progress.setVisibility(View.GONE);
                adLayout.setVisibility(View.VISIBLE);

                titleView.setText(nativeResponse.getTitle());
                promoView.setText(nativeResponse.getPromoText());
                callToActionView.setText(nativeResponse.getCallToAction());
                Float rating = nativeResponse.getRating();
                if (rating != null) {
                    ratingBar.setVisibility(View.VISIBLE);
                    ratingBar.setRating(rating);
                } else {
                    ratingBar.setVisibility(View.GONE);
                }

                Picasso.with(NativeActivity.this).load(nativeResponse.getIconImageUrl()).into(image);

                nativeResponse.registerView(adLayout, callToActionView);
            }

            @Override public void onFail(NativeErrorCode errorCode) {
                Toast.makeText(NativeActivity.this, "Error " + errorCode, Toast.LENGTH_LONG).show();
                progress.setVisibility(View.GONE);
                adLayout.setVisibility(View.GONE);
            }
        });
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        instalNativeAd.destroy();
    }
}
