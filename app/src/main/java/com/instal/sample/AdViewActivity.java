package com.instal.sample;

import android.app.Activity;
import android.os.Bundle;

import com.instal.common.AdParametersBuilder;
import com.instal.common.Gender;
import com.instal.common.LocationAwareness;
import com.instal.mobileads.InstalAdView;


public class AdViewActivity extends Activity {

    private InstalAdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ad_view);
        adView = (InstalAdView) findViewById(R.id.ad_view);
        //optional parameters
        adView.setAdParameters(new AdParametersBuilder().age(18).gender(Gender.MALE).keywords("keyword1, keyword2").locationAwareness(LocationAwareness.TRUNCATED).locationPrecision(2).build());
        adView.setAdUnitId("592");
        adView.loadAd();
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        adView.destroy();
    }
}
