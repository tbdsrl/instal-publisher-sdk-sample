package com.instal.sample;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.instal.discovery.DiscoveryWebView;
import com.instal.discovery.DiscoveryWebViewListener;

public class DiscoveryWebViewCustomActivity extends Activity {
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.discovery_webview);

        final DiscoveryWebView webView = (DiscoveryWebView) findViewById(R.id.discovery_webview);
        final View progress = findViewById(R.id.progress);
        final View errorLayout = findViewById(R.id.error_layout);

        errorLayout.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                webView.load("1");
            }
        });

        webView.setListener(new DiscoveryWebViewListener() {
            @Override public boolean onStartLoading() {
                progress.setVisibility(View.VISIBLE);
                errorLayout.setVisibility(View.GONE);
                return true;
            }

            @Override public void onLoaded() {
                progress.setVisibility(View.GONE);
            }

            @Override public boolean onFail() {
                progress.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);
                Toast.makeText(DiscoveryWebViewCustomActivity.this, "Error loading web page", Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        webView.load("1");
    }
}
