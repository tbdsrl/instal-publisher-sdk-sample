package com.instal.sample;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.instal.nativeads.adapter.InstalAdAdapter;
import com.instal.nativeads.adapter.InstalAdViewHolderFactory;
import com.instal.nativeads.adapter.IntervalAdStrategy;

import java.util.Arrays;


public class NativeListActivity extends Activity {

    private InstalAdAdapter instalAdAdapter;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ListView listView = new ListView(this);
        setContentView(listView);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                Arrays.asList("A", "B", "C", "D", "E", "F", "G", "A", "B", "C", "D", "E", "F", "G", "A", "B", "C", "D", "E", "F", "G")
        );
        instalAdAdapter = new InstalAdAdapter(
                this,
                adapter,
                new IntervalAdStrategy(2, 5),
                new InstalAdViewHolderFactory(R.layout.native_ad, "1254", DemoAdViewHolder.class)
        );
        listView.setAdapter(instalAdAdapter);
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        instalAdAdapter.destroy();
    }
}
