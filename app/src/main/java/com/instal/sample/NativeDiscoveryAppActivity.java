package com.instal.sample;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.instal.nativeads.InstalNativeDiscoveryApp;
import com.instal.nativeads.InstalNativeDiscoveryAppListener;
import com.instal.nativeads.NativeErrorCode;
import com.instal.nativeads.NativeResponse;
import com.instal.nativeads.NativeScreenShot;
import com.squareup.picasso.Picasso;

import java.util.List;


public class NativeDiscoveryAppActivity extends Activity {

    private InstalNativeDiscoveryApp instalNativeDiscoveryApp;
    private Picasso picasso;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discovery);
        picasso = Picasso.with(getApplicationContext());
        callServer(null);
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        if (instalNativeDiscoveryApp != null) {
            instalNativeDiscoveryApp.destroy();
        }
    }

    public void callServer(View v) {
        instalNativeDiscoveryApp = new InstalNativeDiscoveryApp(this, "3");

        instalNativeDiscoveryApp.makeRequest(new InstalNativeDiscoveryAppListener() {
            @Override public void onLoad(List<NativeResponse> nativeResponses) {
                populateApp(nativeResponses, 0, R.id.text_1, R.id.button_1, R.id.image_1, R.id.cover_1, R.id.root_1);
                populateApp(nativeResponses, 1, 0, R.id.button_2, R.id.image_2, R.id.cover_2, R.id.root_2);
                populateApp(nativeResponses, 2, 0, R.id.button_3, R.id.image_3, R.id.cover_3, R.id.root_3);
            }

            @Override public void onFail(NativeErrorCode errorCode) {
                Toast.makeText(NativeDiscoveryAppActivity.this, "Error " + errorCode, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void populateApp(List<NativeResponse> nativeResponses, int pos, int textId, int buttonId, int imageId, int coverId, int rootId) {
        NativeResponse nativeResponse = nativeResponses.get(pos);
        if (textId != 0) {
            ((TextView) findViewById(textId)).setText(nativeResponse.getTitle() + "\n" + nativeResponse.getText());
        }
        ((TextView) findViewById(buttonId)).setText(nativeResponse.getCallToAction());

        picasso.load(nativeResponse.getIconImageUrl()).into((ImageView) findViewById(imageId));
        ImageView cover = (ImageView) findViewById(coverId);
        NativeScreenShot[] screenshots = nativeResponse.getScreenshots();
        if (screenshots != null && screenshots.length > 0) {
            picasso.load(screenshots[0].getUrl()).into(cover);
        } else {
            cover.setImageResource(0);
        }

        nativeResponse.registerView(findViewById(rootId), findViewById(buttonId));
    }
}
