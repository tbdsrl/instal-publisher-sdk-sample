package com.instal.sample;

import android.app.Activity;
import android.os.Bundle;

import com.instal.discovery.DiscoveryWebView;

public class DiscoveryWebViewActivity extends Activity {
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final DiscoveryWebView webView = new DiscoveryWebView(this);
        setContentView(webView);
        webView.load("1");
    }
}
