package com.instal.sample;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.instal.nativeads.FeaturedAlertListener;
import com.instal.nativeads.InstalFeaturedAlert;
import com.instal.nativeads.NativeErrorCode;


public class FeaturedAlertActivity extends Activity {

    private InstalFeaturedAlert featuredAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_featured_alert);
        featuredAlert = new InstalFeaturedAlert(this, "506");
        featuredAlert.setListener(new FeaturedAlertListener() {
            @Override public void onFeaturedAlertLoaded(InstalFeaturedAlert interstitial) {
                showToast("Featured alert loaded");
            }

            @Override public void onFeaturedAlertFailed(InstalFeaturedAlert interstitial, NativeErrorCode errorCode) {
                showToast("Featured alert failed");
            }

            @Override public void onFeaturedAlertShown(InstalFeaturedAlert interstitial) {
                showToast("Featured alert shown");
            }

            @Override public void onFeaturedAlertClicked(InstalFeaturedAlert interstitial) {
                showToast("Featured alert clicked");
                interstitial.load();
            }

            @Override public void onFeaturedAlertDismissed(InstalFeaturedAlert interstitial) {
                showToast("Featured alert dismissed");
                interstitial.load();
            }
        });
        featuredAlert.load();
    }

    private void showToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        featuredAlert.destroy();
    }

    public void showFeaturedAlert(View view) {
        featuredAlert.show();
    }
}
