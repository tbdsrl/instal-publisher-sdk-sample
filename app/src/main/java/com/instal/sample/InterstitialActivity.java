package com.instal.sample;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.instal.common.AdErrorCode;
import com.instal.mobileads.InstalInterstitial;
import com.instal.mobileads.InterstitialAdListener;


public class InterstitialActivity extends Activity {

    private InstalInterstitial interstitial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interstitial);
        interstitial = new InstalInterstitial(this, "2");
        interstitial.setInterstitialAdListener(new InterstitialAdListener() {
            @Override public void onInterstitialLoaded(InstalInterstitial interstitial) {
                showToast("Interstitial loaded");
            }

            @Override public void onInterstitialFailed(InstalInterstitial interstitial, AdErrorCode errorCode) {
                showToast("Interstitial failed");
            }

            @Override public void onInterstitialShown(InstalInterstitial interstitial) {
                showToast("Interstitial shown");
            }

            @Override public void onInterstitialClicked(InstalInterstitial interstitial) {
                showToast("Interstitial clicked");
                interstitial.load();
            }

            @Override public void onInterstitialDismissed(InstalInterstitial interstitial) {
                showToast("Interstitial dismissed");
                interstitial.load();
            }
        });
        interstitial.load();
    }

    private void showToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        interstitial.destroy();
    }

    public void showInterstitial(View view) {
        interstitial.show();
    }
}
