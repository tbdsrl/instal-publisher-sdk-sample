package com.instal.sample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ListView listView = new ListView(this);
        setContentView(listView);
        List<Class<? extends Activity>> classes = Arrays.asList(
                AdViewActivity.class,
                InterstitialActivity.class,
                FeaturedAlertActivity.class,
                NativeActivity.class,
                NativeListActivity.class,
                NativeDiscoveryAppActivity.class,
                DiscoveryWebViewActivity.class,
                DiscoveryWebViewCustomActivity.class);
        final ArrayAdapter<Class<? extends Activity>> adapter = new ArrayAdapter<Class<? extends Activity>>(this, android.R.layout.simple_list_item_1, classes) {
            @Override public View getView(int position, View convertView, ViewGroup parent) {
                TextView view = (TextView) super.getView(position, convertView, parent);
                view.setText(getItem(position).getSimpleName());
                return view;
            }
        };
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(MainActivity.this, adapter.getItem(position)));
            }
        });
    }
}
